package com.acumen.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * The Employee entity.
 */
@ApiModel(description = "The Employee entity.")
@Entity
@Table(name = "employee")
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * The firstname attribute.
     */
    @ApiModelProperty(value = "The firstname attribute.")
    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "hire_date")
    private Instant hireDate;

    @Column(name = "salary")
    private Long salary;

    @Column(name = "commission_pct")
    private Long commissionPct;

    @ManyToOne
    private Department department;

    @OneToMany(mappedBy = "employee")
    @JsonIgnore
    private Set<Job> jobs = new HashSet<>();

    @ManyToOne
    private Employee manager;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     *
     * @param firstName
     * @return
     */
    public Employee firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    /**
     *
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     *
     * @return
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName
     * @return
     */
    public Employee lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    /**
     *
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * @return
     */
    public Employee email(String email) {
        this.email = email;
        return this;
    }

    /**
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     *
     * @param phoneNumber
     * @return
     */
    public Employee phoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    /**
     *
     * @param phoneNumber
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     *
     * @return
     */
    public Instant getHireDate() {
        return hireDate;
    }

    /**
     *
     * @param hireDate
     * @return
     */
    public Employee hireDate(Instant hireDate) {
        this.hireDate = hireDate;
        return this;
    }

    /**
     *
     * @param hireDate
     */
    public void setHireDate(Instant hireDate) {
        this.hireDate = hireDate;
    }

    /**
     *
     * @return
     */
    public Long getSalary() {
        return salary;
    }

    /**
     *
     * @param salary
     * @return
     */
    public Employee salary(Long salary) {
        this.salary = salary;
        return this;
    }

    /**
     *
     * @param salary
     */
    public void setSalary(Long salary) {
        this.salary = salary;
    }

    /**
     *
     * @return
     */
    public Long getCommissionPct() {
        return commissionPct;
    }

    /**
     *
     * @param commissionPct
     * @return
     */
    public Employee commissionPct(Long commissionPct) {
        this.commissionPct = commissionPct;
        return this;
    }

    /**
     *
     * @param commissionPct
     */
    public void setCommissionPct(Long commissionPct) {
        this.commissionPct = commissionPct;
    }

    /**
     *
     * @return
     */
    public Department getDepartment() {
        return department;
    }

    /**
     *
     * @param department
     * @return
     */
    public Employee department(Department department) {
        this.department = department;
        return this;
    }

    /**
     *
     * @param department
     */
    public void setDepartment(Department department) {
        this.department = department;
    }

    /**
     *
     * @return
     */
    public Set<Job> getJobs() {
        return jobs;
    }

    /**
     *
     * @param jobs
     * @return
     */
    public Employee jobs(Set<Job> jobs) {
        this.jobs = jobs;
        return this;
    }

    /**
     *
     * @param job
     * @return
     */
    public Employee addJob(Job job) {
        this.jobs.add(job);
        job.setEmployee(this);
        return this;
    }

    /**
     *
     * @param job
     * @return
     */
    public Employee removeJob(Job job) {
        this.jobs.remove(job);
        job.setEmployee(null);
        return this;
    }

    /**
     *
     * @param jobs
     */
    public void setJobs(Set<Job> jobs) {
        this.jobs = jobs;
    }

    /**
     *
     * @return
     */
    public Employee getManager() {
        return manager;
    }

    /**
     *
     * @param employee
     * @return
     */
    public Employee manager(Employee employee) {
        this.manager = employee;
        return this;
    }

    /**
     *
     * @param employee
     */
    public void setManager(Employee employee) {
        this.manager = employee;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Employee employee = (Employee) o;
        if (employee.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), employee.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Employee{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", email='" + getEmail() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", hireDate='" + getHireDate() + "'" +
            ", salary=" + getSalary() +
            ", commissionPct=" + getCommissionPct() +
            "}";
    }
}
