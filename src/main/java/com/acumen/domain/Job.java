package com.acumen.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Job.
 */
@Entity
@Table(name = "job")
public class Job implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "job_title")
    private String jobTitle;

    @Column(name = "min_salary")
    private Long minSalary;

    @Column(name = "max_salary")
    private Long maxSalary;

    @ManyToOne
    private Employee employee;

    @ManyToMany
    @JoinTable(name = "job_task",
               joinColumns = @JoinColumn(name="jobs_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="tasks_id", referencedColumnName="id"))
    private Set<Task> tasks = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getJobTitle() {
        return jobTitle;
    }

    /**
     *
     * @param jobTitle
     * @return
     */
    public Job jobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
        return this;
    }

    /**
     *
     * @param jobTitle
     */
    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    /**
     *
     * @return
     */
    public Long getMinSalary() {
        return minSalary;
    }

    /**
     *
     * @param minSalary
     * @return
     */
    public Job minSalary(Long minSalary) {
        this.minSalary = minSalary;
        return this;
    }

    /**
     *
     * @param minSalary
     */
    public void setMinSalary(Long minSalary) {
        this.minSalary = minSalary;
    }

    /**
     *
     * @return
     */
    public Long getMaxSalary() {
        return maxSalary;
    }

    /**
     *
     * @param maxSalary
     * @return
     */
    public Job maxSalary(Long maxSalary) {
        this.maxSalary = maxSalary;
        return this;
    }

    /**
     *
     * @param maxSalary
     */
    public void setMaxSalary(Long maxSalary) {
        this.maxSalary = maxSalary;
    }

    /**
     *
     * @return
     */
    public Employee getEmployee() {
        return employee;
    }

    /**
     *
     * @param employee
     * @return
     */
    public Job employee(Employee employee) {
        this.employee = employee;
        return this;
    }

    /**
     *
     * @param employee
     */
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    /**
     *
     * @return
     */
    public Set<Task> getTasks() {
        return tasks;
    }

    /**
     *
     * @param tasks
     * @return
     */
    public Job tasks(Set<Task> tasks) {
        this.tasks = tasks;
        return this;
    }

    /**
     *
     * @param task
     * @return
     */
    public Job addTask(Task task) {
        this.tasks.add(task);
        task.getJobs().add(this);
        return this;
    }

    /**
     *
     * @param task
     * @return
     */
    public Job removeTask(Task task) {
        this.tasks.remove(task);
        task.getJobs().remove(this);
        return this;
    }

    /**
     *
     * @param tasks
     */
    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Job job = (Job) o;
        if (job.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), job.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Job{" +
            "id=" + getId() +
            ", jobTitle='" + getJobTitle() + "'" +
            ", minSalary=" + getMinSalary() +
            ", maxSalary=" + getMaxSalary() +
            "}";
    }
}
