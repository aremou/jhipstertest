/**
 * View Models used by Spring MVC REST controllers.
 */
package com.acumen.web.rest.vm;
