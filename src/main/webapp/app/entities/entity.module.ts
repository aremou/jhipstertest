import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { TestRegionMySuffixModule } from './region-my-suffix/region-my-suffix.module';
import { TestCountryMySuffixModule } from './country-my-suffix/country-my-suffix.module';
import { TestLocationMySuffixModule } from './location-my-suffix/location-my-suffix.module';
import { TestDepartmentMySuffixModule } from './department-my-suffix/department-my-suffix.module';
import { TestTaskMySuffixModule } from './task-my-suffix/task-my-suffix.module';
import { TestEmployeeMySuffixModule } from './employee-my-suffix/employee-my-suffix.module';
import { TestJobMySuffixModule } from './job-my-suffix/job-my-suffix.module';
import { TestJobHistoryMySuffixModule } from './job-history-my-suffix/job-history-my-suffix.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        TestRegionMySuffixModule,
        TestCountryMySuffixModule,
        TestLocationMySuffixModule,
        TestDepartmentMySuffixModule,
        TestTaskMySuffixModule,
        TestEmployeeMySuffixModule,
        TestJobMySuffixModule,
        TestJobHistoryMySuffixModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TestEntityModule {}
